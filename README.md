# SU-TermServ Gitlab Packages Sample

This repository illustrates the packaging required for getting content uploaded to the central terminology server of the MII and NUM using the GitLab Package registry.

## Files in this repository:

```yaml
README.md: this file
.gitlab-ci.yaml: >
    The specification for the GitLab Continuous Integration 
    that builds the package and pushes it to the registry.
build-index.sh: >
    A bash script that generates the index file for the package.
    This script is called by the GitLab CI.
package/:
    package.json: >
        The package specification from which the package is generated, 
        in the format required by the https://hl7.org/fhir/packages.html 
        specification
    .npmrc: >
        The npm configuration file that is used to set the scope 
        for the package
    MII_CS_SUTS_Demo_Codesystem.json: >
        A demo CodeSystem resource
    MII_VS_SUTS_Demo_ValueSet_Arm_Lokal.json: >
        A demo ValueSet resouce that pulls code from 
        the above CodeSystem
    MII_CM_SUTS_Demo_ConceptMap_Arm_Lokal_SnomedCT.json: >
        A demo ConceptMap resource that maps from the 
        codes in the ValueSet to SNOMED CT.
```

## Usage

### Cloning this repository

For creating your own package, you will need to create a *public* GitLab repository that hosts the resources you want to provide to us for upload.

Clone (option A), or fork, disassociate, then clone (option B) the repository, or download the repo as [a compressed file from the release page of this repo](https://gitlab.com/mii-termserv/su-termserv-gitlab-packages-sample/-/releases) (option C). You can use this command to clone this repository with a new name (option A):

```bash
git clone https://gitlab.com/mii-termserv/su-termserv-gitlab-packages-sample.git the-new-name-of-the-package
```

If you *download* (option C) the example from the release, you will need to initialize git using `git init` in the command line.

If you *clone* (option A) this repo, you will need to remove the Git remote. By default, it will be named `origin`, so delete it using `git remote remove origin` in the command line.

If you *download* (option B) the example from the release, OR if you *clone* the repo (option A), you will now need to set the (new) git origin. Create a new GitLab repository using the web interface, and copy the clone url. Issue `git remote add origin <your-git-url>` in the command line. Next, associate the main branch with the new upstream: `git branch --set-upstream-to=origin/main main` (N.B.: your Git may use `master` as the default branch name, and you may want to rename it to `main` using `git branch -m main` before adding the upstream).

If you *forked* the repository, removed the fork relationship, then cloned the new repo (option B), no action for the upstream is needed.

### Adapting the content

Next, open the `package/package.json` file, and change the metadata accordingly:

- change the package scope (in the example, `@mii-termserv`) to the group name of your GitLab. If your repo is available at `https://gitlab.com/example-group/example-repo`, your scope would be `example-group`
- change the package name (the part after the scope) to whatever you desire, it doesn't need to be identical to the repo name, but should be related
- change the version (use SemVer!)
- change the title and description
- change the version of FHIR you reference, if needed
- change author and license (for License, use the [SPDX license IDs](https://spdx.org/licenses/))

In the (hidden) file `package/.npmrc`, change the scope in the first line to your scope.

You do not need to generate or change any of the variables designated by `${}` (e.g. `${CI_SERVER_HOST}` in `.npmrc`, those variables will be automatically substituted using the GitLab CI).

If needed, you may also change this `README.md` as needed.

In the `package/` folder, add the resources you want to bundle in the package.

### Uploading to GitLab

Push your commit to GitLab, and create a release. You can do that using the GitLab command line tool using the command `glab release create <tag>`, where `<tag>` is the version number you set in the `package.json` file. As a one-liner, this command creates a release with the version number from the `package.json` file and no description and no notes:

```bash
glab release create $(jq -rc ".version" package/package.json) --name "" --notes ""
```

## Sample changelog

### 2.0.7 (2024-08-27)

Update index generation script to include the date as index version

### 2.0.6 (2024-08-27)

amend tags with correct license code

### 2.0.5 (2024-08-27)

Tags and narratives

### 2.0.4 (2024-08-27)

fix wrong version of fhir package

### 2.0.3 (2024-07-27)

Move to FHIR R4 instead of R4B

### 2.0.2 (2024-07-11)

Apply the CQF-Scope-Extension to the resources

### 2.0.1 (2024-05-14)

Change GitLab CI to manual trigger using tags/releases.

### 2.0.0 (2024-05-06)

Move `package.json` to the `/package` folder, create a bash script for creating the `.index.json`; update CI accordingly; update readme

### 1.2.1 (2024-04-29)

Fix GitLab CI definitions.

### 1.2.0 (2024-04-29) (yanked!)

Update GitLab CI definition to allow manual starts of the CI job.

### 1.1.0 (2024-03-15)

Move the dependency to the SU-TermServ package which contains the entire R4B terminology.

### 1.0.0 (2023-09-12)

Initial release
